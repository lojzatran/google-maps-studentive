/**
 * pravá část aplikace - formulář pro vytvoření nového místa
 * a seznam uložených míst v aplikaci
 */
$(function () {

    APP = window.APP || {};

    APP.PlaceView = Backbone.View.extend({
        spinner:APP.spinner(),
        initialize:function () {
        },
        el:"#myTabs",
        formTemplate:_.template($('#form-template').html()),
        listTemplate:_.template($('#list-template').html()),
        successTemplate:_.template($('#success-template').html()),
        errorTemplate:_.template($('#error-template').html()),
        confirmTemplate:_.template($('#confirm-template').html()),
        // The DOM events specific to an item.
        events:{
            "click div.saved-places":"showPlaceOnMap",
            "click a#create-place":"createPlace",
            "show a[href='#place-list']":"loadPlaces",
            "change input[name^='address.']":"addressChange",
            "click button[id^='confirm-delete-button-']":"confirmDelete"
        },
        render:function (place) {
            var address = place.get("address");
            $('#place-form').html(this.formTemplate(
                {
                    "name":place.get("name"), "address":address, "notes":place.get("notes")
                }
            ));
        },
        /**
         * pro dané místo udělá značku na mapě
         * @param event
         */
        showPlaceOnMap:function (event) {
            var srcElement = event.srcElement;
            $('.saved-places').css("background-color", "");
            $('button[id^="confirm-delete-button-"]').hide();
            var placeId = srcElement.dataset.placeid;
            var placeModel = APP.placeCollection.get(placeId);
            var latLng = new google.maps.LatLng(placeModel.get("latitude"), placeModel.get("longitude"), false);
            APP.mapView.placeMarker(latLng);
            $(srcElement).css("background-color", "#A8B399");
            $("#confirm-delete-button-" + placeId).show();
        },
        /**
         * uloží nové místo z formuláře na server
         * @param event
         */
        createPlace:function (event) {
            this.startSpinner();
            var transientPlace = APP.placeModel.clone();
            var placeProperties = {
                name:$('#place-name').val(),
                notes:$('#place-notes').val(),
                marker:null
            };
            transientPlace.set(placeProperties);
            if (transientPlace.isValid()) {
                var self = this;
                transientPlace.save({},
                    {
                        success:function (model, response) {
                            self.onSaveSuccess(model, response);
                            self.stopSpinner();
                        }
                    }
                );
            }
        },
        /**
         * callback metoda po úspěšném uložení
         * @param model
         * @param response
         */
        onSaveSuccess:function (model, response) {
            var modelId = response[0].id;
            var message = response[0].message;
            model.set({id:modelId});
            APP.placeCollection.add(model);
            this.displayMessage(message, "success");
            $('#place-form-horizontal')[0].reset();
        },
        /**
         * callback při chybě
         * @param model
         * @param response
         */
        onError:function (model, response) {
            var errorMessage = response.errorMessage;
            this.displayMessage(errorMessage, "error");
            this.stopSpinner();
        },
        /**
         * zobrazí zprávu a po 3 sekundách zmizí
         * @param message
         * @param messageType "success" pro ok zprávy, "error" pro chybové zprávy
         */
        displayMessage:function (message, messageType) {
            switch (messageType) {
                case "success":
                    $('#messages').html(this.successTemplate({
                        "successMessage":message
                    }));
                    break;
                case"error":
                    $('#messages').html(this.errorTemplate({
                        "errorMessage":message
                    }));
                    break;
            }
            var autoclose = setInterval(function () {
                $('#messages').empty();
                clearInterval(autoclose);
            }, 3000);
        },
        /**
         * načte uložená místa ze serveru
         * @param event
         */
        loadPlaces:function (event) {
            this.startSpinner();
            if (APP.placeCollection.length < 2) {       // zajistíme, aby se nenačítaly pořád
                var self = this;
                APP.placeCollection.fetch({success:function (resultCollection) {
                    self.addPlacesToList(resultCollection);
                }});
            } else {
                this.addPlacesToList(APP.placeCollection);
            }
            this.stopSpinner();
        },
        /**
         * přidá uložená místa do kolekce
         * @param resultCollection JSON reprezentace vrácená ze serveru
         */
        addPlacesToList:function (resultCollection) {
            var placesArray = _.sortBy(resultCollection.models, "id");
            var indexNumber = 0;
            var finalTemplates = "";
            while (indexNumber < placesArray.length) {
                var placeModel = placesArray[indexNumber++];
                if (!placeModel.isNew()) {  // zobrazíme v seznamu jen ty místa, která jsou uložena na serveru
                    var address = placeModel.get("address");
                    finalTemplates += this.listTemplate(
                        {
                            "indexNumber":indexNumber, "name":placeModel.get("name"), "placeId":placeModel.get("id"),
                            "address":address, "notes":placeModel.get("notes")
                        }
                    );
                }
            }
            $('#place-list').html(finalTemplates);
        },
        /**
         * automatické vyhledávání na mapě při změně
         * adresy
         * @param event
         */
        addressChange:function (event) {
            this.startSpinner();
            var number = $('#address-number').val();
            var street = $('#address-street').val();
            var city = $('#address-city').val();
            var country = $('#address-country').val();
            APP.mapView.findAddressAndPlaceMarker(number, street, city, country);
        },
        startSpinner:function () {
            this.spinner.spin(this.el);
        },
        stopSpinner:function () {
            this.spinner.stop();
        },
        /**
         * konfirmační okno před smazáním
         * @param event
         */
        confirmDelete:function (event) {
            var placeId = event.srcElement.parentElement.dataset.placeid;
            var confirmTemplate = this.confirmTemplate({placeId:placeId});

            $(confirmTemplate).modal();
            var self = this;
            $('#delete-place-button-' + placeId).on('click', function (event) {
                self.deletePlace(event, self);
            });
            $('#confirmDelete').on("hide", function () {    // remove the event listeners when the dialog is dismissed
                $("#delete-place-button-" + placeId).off("click");
            });

            $('#confirmDelete').on("hidden", function () {  // remove the actual elements from the DOM when fully hidden
                $('#confirmDelete').remove();
            });
            event.stopPropagation();
            event.preventDefault();
        },
        /**
         * smaže uložené místo
         * @param event
         * @param context this v této metodě je window, proto musím zde předat takto objekt placeView
         */
        deletePlace:function (event, context) {
            var placeId = event.srcElement.dataset.placeid;
            var placeModel = APP.placeCollection.get(placeId);
            placeModel.destroy({success:function (model, response) {
                var message = response[0];
                context.displayMessage(message, "success");
                $('#place-' + placeId).remove();
                $('#confirmDelete').modal('hide');
            }, error:function (model, response) {
                var message = response.responseText;
                context.displayMessage(message, "success");
                $('#confirmDelete').modal('hide');
            }});
        }
    });

    APP.placeView = new APP.PlaceView();
})
;