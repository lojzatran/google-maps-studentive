/**
 * Google mapa
 */
$(function () {

    APP = window.APP || {};

    APP.MapModel = Backbone.Model.extend({
        defaults:{
            center:new google.maps.LatLng(-34.397, 150.644),
            zoom:12,
            mapTypeId:google.maps.MapTypeId.ROADMAP
        }
    });

    APP.mapModel = new APP.MapModel;
});