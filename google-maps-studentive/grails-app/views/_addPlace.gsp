<form class="form-horizontal">
    <fieldset>
        <legend>Add Place</legend>

        <div class="control-group">
            <label class="control-label" for="place-name">
                Name
            </label>

            <div class="controls">
                <g:textField name="name" class="input-medium" id="place-name"/>
                <p class="help-block">Name of the place</p>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="address-number">
                House number
            </label>

            <div class="controls">
                <g:textField name="address.number" class="input-medium" id="address-number"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="address-street">
                Street
            </label>

            <div class="controls">
                <g:textField name="address.street" class="input-medium" id="address-street"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="address-city">
                City
            </label>

            <div class="controls">
                <g:textField name="address.city" class="input-medium" id="address-city"/>

            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="address-country">
                Country
            </label>

            <div class="controls">
                <g:textField name="address.country" class="input-medium" id="address-country"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="place-notes">
                Notes
            </label>

            <div class="controls">
                <g:textArea rows="10" cols="10" name="notes" class="input-medium" id="place-notes"/>
                <p class="help-block">Some notes about this place</p>
            </div>
        </div>
    </fieldset>

</form>