commonResources = {
    "place-mvc" {
        dependsOn 'application'
        resource url: '/js/backbone/models/error.js'
        resource url: '/js/backbone/models/place.js'
        resource url: '/js/backbone/views/place.js'
        resource url: '/js/backbone/models/map.js'
        resource url: '/js/backbone/views/map.js'
    }

    "place-mvc-tests" {
        resource url: '/js/libs/jquery-1.8.0.js', disposition: 'head'
        resource url: '/js/tests/jasmine.css', disposition: 'head'
        resource url: '/js/tests/jasmine.js', disposition: 'head'
        resource url: '/js/tests/jasmine-html.js', disposition: 'head'

        resource url: '/js/libs/underscore.js', disposition: 'head'
        resource url: '/js/libs/backbone.js', disposition: 'head'
        resource url: '/js/libs/jquery.json-2.3.js', disposition: 'head'
        resource url: '/js/backbone/models/error.js', disposition: 'head'
        resource url: '/js/backbone/models/place.js', disposition: 'head'
        resource url: '/js/backbone/models/map.js', disposition: 'head'
        resource url: '/js/tests/spec/PlaceSpec.js', disposition: 'head'
    }
}

environments {
    development {
        modules = {
            application {
                dependsOn 'jquery, backbone, bootstrap, spin'
                resource url: '/js/libs/jquery.json-2.3.js'
                resource url: '/js/application.js'
            }

            jquery {
                resource url: '/js/libs/jquery-1.8.0.js', disposition: 'head'
            }

            underscore {
                dependsOn 'jquery'
                resource url: '/js/libs/underscore.js'
            }

            backbone {
                dependsOn 'underscore'
                resource url: '/js/libs/backbone.js'
            }
            spin {
                resource url: '/js/libs/spin.js'
            }
        } << commonResources
    }

    production {
        modules = {
            application {
                dependsOn 'jquery, backbone, bootstrap, spin'
                resource url: '/js/libs/jquery.json-2.3.min.js'
                resource url: '/js/application.js'
            }

            jquery {
                resource url: 'http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js', disposition: 'head'
            }

            underscore {
                dependsOn 'jquery'
                resource url: 'http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.3.3/underscore-min.js'
            }

            backbone {
                dependsOn 'underscore'
                resource url: 'http://cdnjs.cloudflare.com/ajax/libs/backbone.js/0.9.2/backbone-min.js'
            }
            spin {
                resource url: '/js/libs/spin.min.js'
            }
        } << commonResources
    }
}