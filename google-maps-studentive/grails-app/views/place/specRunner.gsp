<!doctype html>
<html>
<head>
    %{--<meta name="layout" content="main"/>--}%
    <title>Jasmine Spec Runner</title>
    <script type="text/javascript">
        var appPath = "${application.contextPath}";
        if (appPath && appPath != "/") {
            window.appPath = appPath;
        }
    </script>
    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCnz-HQaH1LV1ct7pAG00PbYLbu1tWx5xY&sensor=true"></script>
    <r:require module="place-mvc-tests"/>
    <r:layoutResources/>

    <script type="text/javascript">
        (function () {
            var jasmineEnv = jasmine.getEnv();
            jasmineEnv.updateInterval = 1000;

            var trivialReporter = new jasmine.TrivialReporter();

            jasmineEnv.addReporter(trivialReporter);

            jasmineEnv.specFilter = function (spec) {
                return trivialReporter.specFilter(spec);
            };

            var currentWindowOnload = window.onload;

            window.onload = function () {
                if (currentWindowOnload) {
                    currentWindowOnload();
                }
                execJasmine();
            };

            function execJasmine() {
                jasmineEnv.execute();
            }

        })();
    </script>

</head>

<body>
<r:layoutResources/>
</body>
</html>
