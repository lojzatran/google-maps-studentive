/**
 * levá část aplikace - Google mapa
 */
$(function () {

    APP = window.APP || {};

    APP.MapView = Backbone.View.extend({

        id:'map_canvas',
        initialize:function () {
            this.map = new google.maps.Map(this.el, APP.mapModel.attributes);
            this.render();
            this.setCurrentUserLocation();

            var self = this;
            google.maps.event.addListener(this.map, 'click', function (event) {
                self.placeMarker(event.latLng);
            });
        },

        render:function () {
            $('#map_canvas').replaceWith(this.el);
            $('#map_canvas').addClass('span8');
        },
        /**
         * umístí na mapu marker (značku) podle souřadnic
         * @param latLng souřadnice
         * @return {*}
         */
        placeMarker:function (latLng) {
            $('.saved-places').css("background-color", ""); // odznačí se všechny prvky v seznamu míst
            $('button[id^="confirm-delete-button-"]').hide(); // zmizí tlačítko pro mazání místa
            if (APP.placeModel.get("marker")) {
                APP.placeModel.removeMarkerFromMap();
            }
            APP.placeModel = new APP.PlaceModel();

            var marker = new google.maps.Marker({
                position:latLng,
                map:this.map
            });

            var newPlace = APP.placeModel.set({latitude:latLng.lat(), longitude:latLng.lng(), marker:marker}, {silent:true});

            this.map.panTo(latLng);
            this.map.setCenter(latLng);
            this.populatePlaceForm(newPlace);   // toto by se mělo volat pomocí eventu na změnu lat a lng, ale to by házelo zase chybu při validaci, takže to musím takhle
            return newPlace;
        },
        /**
         * předvyplní formulář
         * @param placeModel
         */
        populatePlaceForm:function (placeModel) {
            placeModel.getAddressFromLatLng();
        },
        /**
         * pomocí geolokace vrátí aktuální polohu uživatel
         */
        setCurrentUserLocation:function () {
            if (navigator.geolocation) {
                var self = this;

                navigator.geolocation.getCurrentPosition(function (position) {
                    var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude, false);
                    self.map.setCenter(latLng);
                    self.placeMarker(latLng);
                });
            } else {
                console.log("Geolocation is not supported by this browser");
            }
        },
        /**
         * vyhledá souřadnice adresy a umístí značku
         * na mapě
         * @param number
         * @param street
         * @param city
         * @param country
         */
        findAddressAndPlaceMarker:function (number, street, city, country) {
            var self = this;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address':street + " " + number + ", " + city + " " + country}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latLng = results[0].geometry.location;
                    self.placeMarker(latLng);
                    APP.placeView.stopSpinner();
                } else {
                    APP.placeView.displayMessage("Chyba při hledání místa: " + status, "error");
                    APP.placeView.stopSpinner();
                }
            });
        }

    });

    APP.mapView = new APP.MapView;
});