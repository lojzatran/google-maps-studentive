class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}
        "/placeResource/$id?"(resource: "placeResource")

		"/"(view:"/index")
		"500"(view:'/error')
	}
}
