package google.maps.studentive

class PlaceService {

    public Place get(Long id) {
        Place place = Place.get(id)
        return place
    }

    public List<Place> list() {
        return Place.list()
    }

    public Place save(Place place) {
        place.save()
        return place
    }

    public void delete(Long id) {
        delete(get(id))
    }

    public void delete(Place place) {
        place.delete(flush: true)
    }
}
