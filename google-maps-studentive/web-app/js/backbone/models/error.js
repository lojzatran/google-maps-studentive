$(function () {

    APP = window.APP || {};

    APP.ErrorModel = function () {

        return {
            createErrorModel:function (errors) {
                var errorModel = {
                    errorMessage:"Chyba!",
                    errorSelector:"",
                    errorKey:"error.message"
                };
                $.extend(errorModel, errors);
                return errorModel;
            }
        }
    }();
})
;