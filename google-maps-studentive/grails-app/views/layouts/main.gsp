<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:message code="default.app.title" default="Google Maps Aplikace"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript">
        var appPath = "${application.contextPath}";
        if (appPath && appPath != "/") {
            window.appPath = appPath;
        }
    </script>

    <r:require modules="bootstrap"/>
    <r:layoutResources/>

    <g:layoutHead/>
</head>

<body>
<div class="container">
    <g:layoutBody/>
    <div class="footer" role="contentinfo">
        <g:link controller="place" action="specRunner">
            <g:message code="jasmine.tested.message" default="Jasmine tested"/>
        </g:link>
    </div>
</div>
<r:layoutResources/>
</body>
</html>