package google.maps.studentive

import grails.converters.JSON

class PlaceResourceController {

    def placeService
    def messageSource

    def index() {
        render view: "/index"
    }

    def show() {
        Long id = params.long("id")
        if (id) {
            Place place = placeService.get(id)
            if (place) {
                render place as JSON
            }
            else {
                render status: 404
            }
        } else {
            forward action: "list"
        }
    }

    def list() {
        List<Place> placeList = placeService.list()
        List placeJSON = []
        placeList.each {place ->
            placeJSON.add([
                    id: place.id, name: place.name, latitude: place.latitude, longitude: place.longitude,
                    address: [
                            number: place.address.number ?: "",
                            street: place.address.street,
                            city: place.address.city,
                            country: place.address.country
                    ]
            ])
        }
        render placeJSON as JSON
    }

    def save() {
        Place place = new Place()
        place.properties = params
        placeService.save(place)
        if (place.id) {
            List placeJSON = [[
                    id: place.id,
                    message: messageSource.getMessage("place.created.message",
                            [place.name, place.id] as Object[], Locale.getDefault())
            ]]
            render placeJSON as JSON
        } else {
            render status: 400
        }
    }

    def update() {
        log.info("update")
    }

    def delete() {
        Long placeId = params.long("id")
        if (placeId) {
            placeService.delete(placeId)
            String message = messageSource.getMessage("place.deleted.message", [placeId] as Object[], Locale.getDefault())
            render status: 200, text: [message] as JSON
        } else {
            render status: 400
        }
    }
}
