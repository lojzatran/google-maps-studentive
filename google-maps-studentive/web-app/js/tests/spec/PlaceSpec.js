/**
 * Created with IntelliJ IDEA.
 * User: manhdung
 * Date: 9/1/12
 * Time: 10:52 AM
 * To change this template use File | Settings | File Templates.
 */

describe('PlaceModel', function () {
    var placeModelTestValues = {
        name:"",
        address:{
            number:0,
            street:"",
            city:"",
            country:""
        },
        latitude:0,
        longitude:0,
        notes:"",
        marker:null
    };

    it("Can initialize with default values", function () {
        var testPlace = new APP.PlaceModel();
        expect(testPlace.get("name")).toEqual("");
        expect(testPlace.get("address").number).toEqual(0);
        expect(testPlace.get("address").street).toEqual("");
        expect(testPlace.get("address").city).toEqual("");
        expect(testPlace.get("address").country).toEqual("");
        expect(testPlace.get("latitude")).toEqual(0);
        expect(testPlace.get("longitude")).toEqual(0);
        expect(testPlace.get("notes")).toEqual("");
        expect(testPlace.get("marker")).toBeNull();
    });

    it("Can get address from lat & lng", function () {
        APP.placeView = {
            render:function () {
            }
        };
        spyOn(APP.placeView, 'render');

        var testPlace = new APP.PlaceModel({latitude:50.0755381, longitude:14.4378005}); // Praha
        testPlace.getAddressFromLatLng();
        waitsFor(function () {             // čekáme na odpověď z Google Maps API
            return testPlace.get("address").number != 0 && testPlace.get("address").street !== ""
                && testPlace.get("address").city !== "" && testPlace.get("address").country !== "";
        }, "No response from Google Maps API", 5000);

        runs(function () {
            expect(testPlace.get("address").street).toBeTruthy();
            expect(testPlace.get("address").city).toBeTruthy();
            expect(testPlace.get("address").country).toBeTruthy();
            expect(APP.placeView.render).toHaveBeenCalled();
            expect(APP.placeView.render).toHaveBeenCalledWith(testPlace);
        });
    });

    it('Can contain custom validation rules, and will trigger an error event on failed validation.', function () {
        var errorCallback = jasmine.createSpy('-error event callback-');

        var placeModel = new APP.PlaceModel();
        placeModel.off('error');    // smazat případné defaultní callbacky

        placeModel.on('error', errorCallback);

        var customTestValues = $.extend(true, {}, placeModelTestValues);
        delete customTestValues.name;
        placeModel.set(customTestValues);

        var errorArgs = errorCallback.mostRecentCall.args;

        expect(errorArgs).toBeDefined();
        expect(errorArgs[0]).toBe(placeModel);
        expect(errorArgs[1].errorMessage).toBe('Chybí název místa');
    });

});