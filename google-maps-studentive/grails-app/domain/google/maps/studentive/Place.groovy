package google.maps.studentive

class Place {

    String name

    Address address
    static embedded = ['address']

    Double latitude
    Double longitude

    String notes

    static constraints = {
    }
}

class Address {

    Integer number
    String street
    String city
    String country

    static constraints = {
        number nullable: true
    }
}