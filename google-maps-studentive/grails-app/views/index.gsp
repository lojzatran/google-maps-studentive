<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <style>
    #map_canvas {
        margin-top: 3%;
        height: 500px;
    }

    #map_canvas label {
        width: auto;
        display: inline;
    }

    #map_canvas img {
        max-width: none;
        max-height: none;
    }
    </style>
    <r:require module="place-mvc"/>
    <g:if env="production">
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-34591701-1']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
    </g:if>
</head>

<body>

<div class="row">
    <div id="map_canvas" class="span4"></div>

    <div class="tabbable span4" id="myTabs">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#place-form" data-toggle="tab">
                <g:message code="default.add.label"/>
            </a></li>
            <li><a href="#place-list" data-toggle="tab">
                <g:message code="default.list.label"/>
            </a></li>
        </ul>

        <div class="tab-content ">
            <div id="messages"></div>

            <div class="tab-pane active" id="place-form">
            </div>

            <div class="tab-pane" id="place-list">

            </div>
        </div>
    </div>
    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCnz-HQaH1LV1ct7pAG00PbYLbu1tWx5xY&sensor=true">
    </script>
</div>

%{--Templates--}%
<script type="text/template" id="form-template">

    <form class="form-horizontal" id="place-form-horizontal">
        <fieldset>
            <legend>
                <g:message code="default.create.label"/>
            </legend>

            <div class="control-group">
                <label class="control-label" for="place-name">
                    <g:message code="place.name.label"/>
                </label>

                <div class="controls">
                    <g:textField name="name" class="input-medium" id="place-name" value="{{name}}"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="address-number">
                    <g:message code="address.number.label"/>
                </label>

                <div class="controls">
                    <g:textField name="address.number" value="{{address.number}}"
                                 class="input-medium" id="address-number"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="address-street">
                    <g:message code="address.street.label"/>
                </label>

                <div class="controls">
                    <g:textField name="address.street" value="{{address.street}}"
                                 class="input-medium" id="address-street"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="address-city">
                    <g:message code="address.city.label"/>
                </label>

                <div class="controls">
                    <g:textField name="address.city" value="{{address.city}}"
                                 class="input-medium" id="address-city"/>

                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="address-country">
                    <g:message code="address.country.label"/>
                </label>

                <div class="controls">
                    <g:textField name="address.country" value="{{address.country}}"
                                 class="input-medium" id="address-country"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="place-notes">
                    <g:message code="place.notes.label"/>
                </label>

                <div class="controls">
                    <g:textArea rows="10" cols="10" name="notes" value="{{notes}}"
                                class="input-medium" id="place-notes"/>
                </div>
            </div>
        </fieldset>

        <a href="#" id="create-place" class="btn">
            <g:message code="default.button.create.label"/>
        </a>

    </form>
</script>

<script type="text/template" id="error-template">
    <div class="alert alert-error">
        <a href="#" class="close" data-dismiss="alert">×</a>
        {{errorMessage}}
    </div>
</script>

<script type="text/template" id="success-template">
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert">×</a>
        {{successMessage}}
    </div>
</script>

<script type="text/template" id="list-template">
    <div id="place-{{placeId}}" class="well saved-places" data-placeId="{{placeId}}">
        <button id="confirm-delete-button-{{placeId}}" data-toggle="modal" data-target="#confirmDelete"
                class="pull-right btn btn-danger" style="display: none;">&times;</button>
        <span class="label label-success">{{indexNumber}}.</span>
        <g:message code="place.name.label" default="Název"/>: {{name}};
        <g:message code="place.address.label"
                   default="Adresa"/>: {{address.street}} {{address.number}}, {{address.city}}
    </div>
</script>

<script type="text/template" id="confirm-template">
    <div class="modal" style="display: none; width: 14%; top: 60%; left: 67%;" id="confirmDelete"
         tabindex="-1" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
        <div class="modal-body">
            <p>
                <g:message code="default.delete.confirm.label"/>
            </p>
        </div>

        <div class="modal-footer">
            <button class="btn btn-small" data-dismiss="modal" aria-hidden="true">
                <g:message code="default.no.label"/>
            </button>
            <button class="btn btn-small btn-danger" id="delete-place-button-{{placeId}}"
                    data-placeid="{{placeId}}">
                <g:message code="default.yes.label"/>
            </button>
        </div>
    </div>
</script>

</body>

</html>