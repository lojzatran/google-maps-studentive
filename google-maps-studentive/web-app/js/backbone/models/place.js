/**
 * model pro místo
 */
$(function () {

    APP = window.APP || {};


    APP.PlaceModel = Backbone.Model.extend({
        urlRoot:function () {
            return window.appPath + "/placeResource/";
        },
        initialize:function () {
            this.on("error", this.showErrors);
        },
        defaults:{
            id:null,
            name:"",
            address:{
                number:0,
                street:"",
                city:"",
                country:""
            },
            latitude:0,
            longitude:0,
            notes:"",
            marker:null
        },
        /**
         * smaže označení místa z mapy
         */
        removeMarkerFromMap:function () {
            var marker = this.get("marker");
            marker.setMap(null);
        },
        /**
         * vyhledá a vrátí adresu ze souřadnic
         */
        getAddressFromLatLng:function () {
            var lat = this.get("latitude");
            var lng = this.get("longitude");
            var latLng = new google.maps.LatLng(lat, lng);
            var geocoder = new google.maps.Geocoder();
            var self = this;
            geocoder.geocode({'latLng':latLng}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var resultJson = $.evalJSON($.toJSON(results[0]));
                        self.parseAddress(resultJson);
                        APP.placeView.render(self);
                    }
                } else {
                    console.log("Geocoder failed due to: " + status);
                    console.log($.toJSON(status));
                }
            });
        },
        /**
         * z JSON vrácené ze servery parsuje adresu
         * a uloží do modelu
         * @param resultJson
         * @return {Object}
         */
        parseAddress:function (resultJson) {
            var addressArray = resultJson.address_components;
            var self = this;
            var currentAddress = {};
            $.each(addressArray, function (index, element) {
                var types = element.types[0];
                switch (types) {
                    case "street_number":
                        currentAddress.number = element.long_name;
                        break;
                    case "route":
                        currentAddress.street = element.long_name;
                        break;
                    case "locality":
                        currentAddress.city = element.long_name;
                        break;
                    case "country":
                        currentAddress.country = element.long_name;
                        break;
                }
            });
            self.set({address:currentAddress}, {silent:true});
            return currentAddress;
        },
        validate:function (attrs) {
            if (!attrs.name) {
                return APP.ErrorModel.createErrorModel({errorMessage:"Chybí název místa"});
            }
        },
        showErrors:function (model, errorObject) {
            APP.placeView.onError(this, errorObject);
            return false;
        }
    });

    APP.placeModel = new APP.PlaceModel();

    /**
     * kolekce míst
     * @type {*}
     */
    APP.PlaceCollection = Backbone.Collection.extend({
        initialize:function () {
        },
        model:APP.PlaceModel,
        url:window.appPath + "/placeResource"
    });

    APP.placeCollection = new APP.PlaceCollection();

});